import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { AuthorDto } from '../dto/create-author.dto';
import { AuthorInput } from '../input/create-author.input';
import { BookDto } from 'src/dto/create-book.dto';

@Resolver(() => AuthorDto)
export class AuthorResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [AuthorDto])
  public async authors(): Promise<AuthorDto[]> {
    return this.repoService.findAuthors();
  }

  @ResolveField('books', () => [BookDto])
  public async author(@Parent() author: AuthorDto): Promise<BookDto[]> {
    const { id } = author;
    return this.repoService.bookRepo.find({ authorId: id });
  }

  @Mutation(type => AuthorDto)
  public async addAuthor(
    @Args('input') input: AuthorInput,
  ): Promise<AuthorDto> {
    return this.repoService.addAuthor(input);
  }
}
