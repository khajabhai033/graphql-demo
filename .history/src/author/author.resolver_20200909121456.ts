import { Resolver, Query } from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { AuthorDto } from '../dto/create-author.dto';

@Resolver()
export class AuthorResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [AuthorDto])
  public async authors(): Promise<AuthorDto[]> {
    return this.repoService.findAuthors();
  }
}
