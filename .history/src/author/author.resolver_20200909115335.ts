import { Resolver, Query } from '@nestjs/graphql';
import { RepoService } from './repo.service';
import { RepoModule } from './repo.module';
import { Author } from '../models/author.entity';
import { Inject, forwardRef } from '@nestjs/common';

@Resolver()
export class AuthorResolver {
  constructor(
    @Inject(forwardRef(() => RepoService)) private repoService: RepoService,
  ) {}

  @Query(() => [Author])
  public async authors(): Promise<Author[]> {
    return this.repoService.authorRepo.find();
  }
}
