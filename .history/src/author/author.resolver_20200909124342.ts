import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { AuthorDto } from '../dto/create-author.dto';
import { AuthorInput } from '../input/create-author.input';

@Resolver(() => AuthorDto)
export class AuthorResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [AuthorDto])
  public async authors(): Promise<AuthorDto[]> {
    return this.repoService.findAuthors();
  }

  @Mutation(type => AuthorDto)
  public async addAuthor(
    @Args('input') input: AuthorInput,
  ): Promise<AuthorDto> {
    return this.repoService.addAuthor(input);
  }
}
