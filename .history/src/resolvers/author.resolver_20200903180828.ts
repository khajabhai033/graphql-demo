import { Resolver } from '@nestjs/graphql';
import { RepoService } from 'src/repo/repo.service';

@Resolver()
export class AuthorResolver {
  constructor(private repoService: RepoService) {}
  @Query(() => [Author])
  public async authors(): Promise<Author[]> {
    return this.repoService.authorRepo.find();
  }
}
