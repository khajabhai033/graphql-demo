import { Resolver } from '@nestjs/graphql';
import { RepoService } from 'src/repo/repo.service';

@Resolver()
export class AuthorResolver {
  constructor(private repoService: RepoService) {}
}
