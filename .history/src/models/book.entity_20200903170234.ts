import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Author } from './author.entity';
import { Field, ObjectType } from 'type-graphql';

@Entity()
export class Book {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  title: string;

  @Column()
  genre: string;

  @ManyToOne(
    () => Author,
    author => author.id,
  )
  author: Promise<Author>;
}
