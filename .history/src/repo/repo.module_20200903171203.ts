import { Module } from '@nestjs/common';
import { RepoService } from './repo.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from '../models/book.entity';
import { Author } from '../models/author.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Author, Book])],
  providers: [RepoService],
})
export class RepoModule {}
