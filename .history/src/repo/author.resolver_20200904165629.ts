import { Resolver, Query } from '@nestjs/graphql';
import { RepoService } from './repo.service';
import { Author } from '../models/author.entity';

@Resolver()
export class AuthorResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [Author])
  public async authors(): Promise<Author[]> {
    return this.repoService.authorRepo.find();
  }
}
