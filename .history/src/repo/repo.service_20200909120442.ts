import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from '../models/book.entity';
import { Author } from '../models/author.entity';

@Injectable()
export class RepoService {
  constructor(
    @InjectRepository(Book) public bookRepo: Repository<Book>,
    @InjectRepository(Author) public authorRepo: Repository<Author>,
  ) {}
}
