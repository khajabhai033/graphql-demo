import { Resolver, Query } from '@nestjs/graphql';
import { RepoService } from './repo.service';
import { Author } from '../models/author.entity';
import { Inject } from '@nestjs/common';

@Resolver()
export class AuthorResolver {
  constructor(@Inject(RepoService) private repoService: RepoService) {}

  @Query(() => [Author])
  public async authors(): Promise<Author[]> {
    return this.repoService.authorRepo.find();
  }
}
