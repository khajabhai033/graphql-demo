import { Module } from '@nestjs/common';
import { RepoService } from './repo.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from '../models/book.entity';
import { Author } from '../models/author.entity';
import { AuthorResolver } from './author.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([Author, Book]), AuthorResolver],
  providers: [RepoService, AuthorResolver],
  exports: [RepoService, AuthorResolver],
})
export class RepoModule {}
