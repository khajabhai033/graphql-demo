import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from '../models/book.entity';
import { Author } from '../models/author.entity';
import { BookDto } from '../dto/create-book.dto';
import { AuthorDto } from 'src/dto/create-author.dto';
import { BookInput } from 'src/input/create-book.input';
import { AuthorInput } from 'src/input/create-author.input';

@Injectable()
export class RepoService {
  constructor(
    @InjectRepository(Book) public bookRepo: Repository<Book>,
    @InjectRepository(Author) public authorRepo: Repository<Author>,
  ) {}

  async findBooks(): Promise<BookDto[]> {
    return await this.bookRepo.find();
  }

  async findAuthors(): Promise<AuthorDto[]> {
    return await this.authorRepo.find();
  }

  async addBook(input: BookInput): Promise<BookDto> {
    return await this.bookRepo.save(input);
  }

  async addAuthor(input: AuthorInput): Promise<AuthorDto> {
    return await this.authorRepo.save(input);
  }
}
