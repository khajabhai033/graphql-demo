import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from '../models/book.entity';
import { Author } from '../models/author.entity';
import { BookDto } from '../dto/create-book.dto';
import { AuthorDto } from 'src/dto/create-author.dto';

@Injectable()
export class RepoService {
  constructor(
    @InjectRepository(Book) public bookRepo: Repository<Book>,
    @InjectRepository(Author) public authorRepo: Repository<Author>,
  ) {}

  async findBooks(): Promise<BookDto[]> {
    return await this.bookRepo.find();
  }

  async findAuthors(): Promise<AuthorDto[]> {
    return await this.authorRepo.find();
  }
}
