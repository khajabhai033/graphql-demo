import { Resolver, Query } from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { BookDto } from '../dto/create-book.dto';

@Resolver()
export class BookResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [BookDto])
  public async authors(): Promise<BookDto[]> {
    return this.repoService.findBooks();
  }
}
