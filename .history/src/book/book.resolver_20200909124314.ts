import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { BookDto } from '../dto/create-book.dto';
import { BookInput } from '../input/create-book.input';
import { AuthorDto } from 'src/dto/create-author.dto';
import { Book } from 'src/models/book.entity';

@Resolver(() => Book)
export class BookResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [BookDto])
  public async books(): Promise<BookDto[]> {
    return this.repoService.findBooks();
  }

  @ResolveField('author', () => AuthorDto)
  public async author(@Parent() book: BookDto): Promise<AuthorDto> {
    return this.repoService.authorRepo.findOne(book.author);
  }

  @Mutation(type => BookDto)
  public async addBook(@Args('input') input: BookInput): Promise<BookDto> {
    return this.repoService.addBook(input);
  }
}
