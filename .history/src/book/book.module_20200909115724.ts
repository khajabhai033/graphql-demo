import { Module } from '@nestjs/common';
import { BookResolver } from './book.resolver';

@Module({
  providers: [BookResolver],
  exports: [BookResolver],
})
export class BookModule {}
