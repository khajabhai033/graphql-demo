import { Resolver } from '@nestjs/graphql';

@Resolver()
export class BookResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [Author])
  public async authors(): Promise<Author[]> {
    return this.repoService.authorRepo.find();
  }
}
