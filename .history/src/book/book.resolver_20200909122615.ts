import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { BookDto } from '../dto/create-book.dto';
import { Book } from '../input/create-book.input';

@Resolver()
export class BookResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [BookDto])
  public async books(): Promise<BookDto[]> {
    return this.repoService.findBooks();
  }

  @Mutation()
  public async addBook(@Args('input') input: Book) {
    return this.repoService.addBook(input);
  }
}
