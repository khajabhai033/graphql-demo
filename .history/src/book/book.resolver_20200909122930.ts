import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { BookDto } from '../dto/create-book.dto';
import { BookInput } from '../input/create-book.input';

@Resolver()
export class BookResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [BookDto])
  public async books(): Promise<BookDto[]> {
    return this.repoService.findBooks();
  }

  @Mutation(type => BookDto)
  public async addBook(@Args('input') input: BookInput): Promise<BookDto> {
    return this.repoService.addBook(input);
  }
}
