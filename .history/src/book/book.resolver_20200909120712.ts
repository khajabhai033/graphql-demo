import { Resolver, Query } from '@nestjs/graphql';
import { RepoService } from '../repo/repo.service';
import { Book } from '../dto/create-book.dto';

@Resolver()
export class BookResolver {
  constructor(private repoService: RepoService) {}

  @Query(() => [Book])
  public async authors(): Promise<Book[]> {
    return this.repoService.bookRepo.find();
  }
}
