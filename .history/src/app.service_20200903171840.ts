import { Injectable } from '@nestjs/common';
import { RepoService } from './repo/repo.service';

@Injectable()
export class AppService {
  constructor(private repoService: RepoService) {}
  getHello(): string {
    return 'Hello World!';
  }
}
