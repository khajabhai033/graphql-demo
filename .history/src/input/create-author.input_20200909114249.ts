import { Book } from '../models/book.entity';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class Author {
  @Field()
  name: string;

  @Field()
  age: number;
}
