import { Book } from '../models/book.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Author {
  @Field()
  name: string;

  @Field()
  age: number;

  @Field()
  books: Book[];
}
