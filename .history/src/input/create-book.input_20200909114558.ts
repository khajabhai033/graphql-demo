import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class Book {
  @Field()
  title: string;

  @Field()
  genre: string;

  @Field()
  authorId: number;
}
