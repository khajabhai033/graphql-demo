import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class AuthorInput {
  @Field()
  name: string;

  @Field()
  age: number;
}
