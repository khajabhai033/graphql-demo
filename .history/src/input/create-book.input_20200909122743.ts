import { Field, InputType, Int } from '@nestjs/graphql';

@InputType()
export class BookInput {
  @Field()
  title: string;

  @Field()
  genre: string;

  @Field(() => Int)
  authorId: number;
}
