import { Book } from '../models/book.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Author {
  @Field()
  id: number;

  @Field()
  name: string;

  @Field()
  age: number;

  @Field(() => [Book])
  books: Book[];
}
