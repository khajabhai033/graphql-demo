import { Author } from './create-author.dto';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BookDto {
  @Field()
  id: number;

  @Field()
  title: string;

  @Field()
  genre: string;

  @Field()
  author: Author;
}
