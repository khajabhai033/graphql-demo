import { AuthorDto } from './create-author.dto';
import { Field, ObjectType, ID } from '@nestjs/graphql';

@ObjectType()
export class BookDto {
  @Field(() => ID)
  id: number;

  @Field()
  title: string;

  @Field()
  genre: string;

  @Field(() => AuthorDto)
  author: AuthorDto;
}
