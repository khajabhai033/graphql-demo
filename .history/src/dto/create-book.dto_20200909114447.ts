import { Author } from './create-author.dto';
import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class Book {
  @Field()
  id: number;

  @Field()
  title: string;

  @Field()
  genre: string;

  @Field()
  author: Author;
}
