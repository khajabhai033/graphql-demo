import { Book } from './create-book.dto';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AuthorDto {
  @Field()
  id: number;

  @Field()
  name: string;

  @Field()
  age: number;

  @Field(() => [Book])
  books: Book[];
}
