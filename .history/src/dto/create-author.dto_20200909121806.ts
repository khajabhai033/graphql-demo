import { BookDto } from './create-book.dto';
import { Field, ObjectType, ID } from '@nestjs/graphql';

@ObjectType()
export class AuthorDto {
  @Field(() => ID)
  id: number;

  @Field()
  name: string;

  @Field()
  age: number;

  @Field(() => [BookDto])
  books: BookDto[];
}
