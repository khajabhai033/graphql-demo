import { Author } from '../models/author.entity';
import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class Book {
  @Field()
  id: number;

  @Field()
  title: string;

  @Field()
  genre: string;
  @Field()
  author: Promise<Author>;
}
