import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RepoModule } from './repo/repo.module';
import { GraphQLModule } from '@nestjs/graphql';
import { AuthorResolver } from './resolvers/author.resolver';

const graphQLImports = [AuthorResolver];
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    RepoModule,
    ...graphQLImports,
    GraphQLModule.forRoot({ autoSchemaFile: 'schema.gql', playground: true }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
