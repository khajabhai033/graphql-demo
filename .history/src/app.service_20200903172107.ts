import { Injectable } from '@nestjs/common';
import { RepoService } from './repo/repo.service';

@Injectable()
export class AppService {
  constructor(private repoService: RepoService) {}
  async getHello(): Promise<string> {
    return `Total Books are ${await this.repoService.bookRepo.count()}`;
  }
}
