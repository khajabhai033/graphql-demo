import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RepoModule } from './repo/repo.module';
import { GraphQLModule } from '@nestjs/graphql';
import { AuthorResolver } from './repo/author.resolver';
import { AuthorModule } from './author/author.module';
import { BookModule } from './book/book.module';

const graphQLImports = [AuthorResolver];
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    RepoModule,
    ...graphQLImports,
    GraphQLModule.forRoot({ autoSchemaFile: 'schema.gql', playground: true }),
    AuthorModule,
    BookModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
