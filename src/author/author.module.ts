import { Module } from '@nestjs/common';
import { AuthorResolver } from './author.resolver';
import { RepoModule } from 'src/repo/repo.module';

@Module({
  imports: [RepoModule],
  providers: [AuthorResolver],
  exports: [AuthorResolver],
})
export class AuthorModule {}
