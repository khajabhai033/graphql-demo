import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { AuthorModule } from './author/author.module';
import { BookModule } from './book/book.module';
import { DbConfig } from './typeOrm.config';

@Module({
  imports: [
    TypeOrmModule.forRoot(DbConfig),
    GraphQLModule.forRoot({ autoSchemaFile: 'schema.gql', playground: true }),
    AuthorModule,
    BookModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
