import { Module } from '@nestjs/common';
import { BookResolver } from './book.resolver';
import { RepoModule } from 'src/repo/repo.module';

@Module({
  imports: [RepoModule],
  providers: [BookResolver],
  exports: [BookResolver],
})
export class BookModule {}
